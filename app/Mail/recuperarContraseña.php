<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class recuperarContraseña extends Mailable
{
    public $nuevacontraseña;
    use Queueable, SerializesModels;

    public $subjet = "Recuperar contraseña";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $nuevacontraseña)
    {
        //
        $this->nuevacontraseña = $nuevacontraseña;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.recuperar');
    }
}
