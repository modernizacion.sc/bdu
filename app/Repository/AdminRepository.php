<?php

namespace App\Repository;

use Illuminate\Support\Facades\DB;

/**
 * Repositorio de acceso a datos solamente referido a la gestion de grupos y rutas/metodos
 */
class AdminRepository
{


    //Determina si un usuario pertenece a un grupo.
     
    public static function perteneceGrupo($usuarioId, $nombreGrupo)
    {
        $result = DB::select(
            'Select u.*,g.* from users u left join usuarios_grupos ug on u.id=ug.id_usuario left join grupos g on ug.id_grupo=g.id_grupo
        where u.id = :usuarioId and g.nombre_grupo = :nombreGrupo',
            ["usuarioId" => $usuarioId, "nombreGrupo" => $nombreGrupo]
        );
        return $result;
    }

    
    //Determina si un usuario tiene acceso a la ruta
     
    public static function accesoRuta($usuarioId, $ruta, $metodo)
    {
        // Obtener los grupos a los que pertenece el usuario
        // Obtener las rutas vinculadas a esos grupos
        // Verificar si la ruta indicada esta dentro de las rutas habilitadas

        $result = DB::select(
            'select * from grupos g, grupos_rutas_metodos grm, usuarios_grupos ug, rutas r where
        ug.id_usuario = :usuarioId and g.id_grupo = ug.id_grupo and g.id_grupo = grm.id_grupo and
        grm.id_ruta = r.id_ruta and r.ruta = :ruta and grm.metodo_acceso = :metodo',
            ["usuarioId" => $usuarioId, "ruta" => $ruta, "metodo" => $metodo]
        );
        return $result;
    }

    
    //Retorna las rutas a las que tiene acceso el usuario
     
    public static function listarRutasUsuario($idUsuario)
    {
        return DB::select('select r.ruta, grm.descripcion, grm.metodo_acceso, grm.flag_habilitado from grupos g, grupos_rutas_metodos grm, usuarios_grupos ug, rutas r where
        ug.id_usuario = :usuarioId and g.id_grupo = ug.id_grupo and g.id_grupo = grm.id_grupo and
        grm.id_ruta = r.id_ruta', ["usuarioId" => $idUsuario]);
    }




    /**
     * ********************************** GRUPOS *****************************
     */

    
    //Inserta un grupo en la base de datos
     
    public static function crearGrupo($datos)
    {
        $result = DB::insert(
            'insert into grupos (nombre_grupo, descripcion) values (:nombre,:descripcion)',
            ["nombre" => $datos["nombre"], "descripcion" => $datos["descripcion"]]
        );
        return $result;
    }

    
    //Inserta un grupo en la base de datos
     
    public static function actualizarGrupo($datos, $id)
    {
        $result = DB::update(
            'update grupos set nombre_grupo = :nombre, descripcion = :descripcion where id_grupo = :id',
            ["id" => $id, "nombre" => $datos["nombre_grupo"], "descripcion" => $datos["descripcion"]]
        );
        return $result;
    }

    
    //Borra un grupo.
     
    public static function borrarGrupo($id)
    {
        $result = DB::delete('delete from grupos where id_grupo = :id', ["id" => $id]);
        return $result;
    }


    //Mostrar todos los Grupos
    
    public static function listarGrupos()
    {
        return DB::select('select * from grupos');
    }


    //Obtener un grupo a partir de un ID

    public static function obtenerGrupo($id)
    {
        return DB::select('select * from grupos where id_grupo = ?', [$id]);
    }
    public static function obtenerGrupoNombre($nombreGrupo)
    {
        return DB::select('select * from grupos where nombre_grupo = ?', [$nombreGrupo]);
    }

    


    /**
     * ********************************** RUTAS *****************************
     */

    
    //Crear una Ruta en la base de datos
     
    public static function crearRuta($datos)
    {
        $result = DB::insert(
            'insert into rutas (ruta, descripcion) values (:ruta,:descripcion)',
            ["ruta" => $datos["ruta"], "descripcion" => $datos["descripcion"]]
        );
        return $result;
    }

    
    //Actualizar una ruta existente
     
    public static function actualizarRuta($datos, $id)
    {
        $result = DB::update(
            'update rutas set ruta = :ruta, descripcion = :descripcion where id_ruta = :id',
            ["id" => $id, "ruta" => $datos["ruta"], "descripcion" => $datos["descripcion"]]
        );
        return $result;
    }

    
    //Elimina un ruta por metodo de la tabla grupos_rutas_metodos, segun la PK de la tabla.
     
    public static function borrarRutaMetodo($id)
    {
        $result = DB::delete('delete from grupos_rutas_metodos where id_gruporutametodo = :id', ["id" => $id]);
        return $result;
    }

    
    //Elimina un ruta por metodo de la tabla grupos_rutas_metodos, segun el id_grupo, id_ruta y el metodo_acceso.
     
    public static function borrarRutaMetodo1($idGrupo, $idRuta, $metodoAcceso)
    {
        $result = DB::delete('delete from grupos_rutas_metodos where id_grupo = :idGrupo and id_ruta = :idRuta and metodo_acceso = :metodoAcceso', ["idGrupo" => $idGrupo, "idRuta" => $idRuta, "metodoAcceso" => $metodoAcceso]);
        return $result;
    }

    
    //Elimina un ruta generica de la tabla rutas
     
    public static function borrarRuta($id)
    {
        $result = DB::delete('delete from rutas where id_ruta = :id', ["id" => $id]);
        return $result;
    }


    //Mostrar todas las rutas disponibles

    public static function listarRutas()
    {
        return DB::select('select * from rutas');
    }


    //Obtener una ruta de la tabla rutas mediante su ID

    public static function obtenerRuta($id)
    {
        return DB::select('select * from rutas where id_ruta = ?', [$id]);
    }




    /**
     * ********************************** USUARIOS GRUPOS *****************************
     */

    //Agrega un usuario a un grupo.

    public static function crearUsuarioGrupo($idGrupo, $idUsuario, $datos)
    {
        $result = DB::insert(
            'insert into usuarios_grupos (id_usuario, id_grupo, flag_habilitado) values (:idUsuario, :idGrupo, :flagHabilitado)',
            ["idUsuario" => $idUsuario, "idGrupo" => $idGrupo, "flagHabilitado" => $datos["flagHabilitado"]]
        );
        return $result;
    }

    
    //Elimina un usuario de un grupo

    public static function eliminarUsuarioGrupo($idUsuario, $idGrupo)
    {
        $result = DB::delete(
            'delete from usuarios_grupos where id_usuario = :idUsuario and id_grupo = :idGrupo',
            ["idUsuario" => $idUsuario, "idGrupo" => $idGrupo]

        );
        return $result;
    }


    //Obtener todos los usuarios con Grupo

    public static function obtenerUsuariosGrupos()
    {
        return DB::select('select * from usuarios_grupos');
    }


    //Obtener todos los Usuarios de un Grupo

    public static function obtenerUsuariosDeGrupo($idGrupo)
    {
        return DB::select('select u.id,u.nombre,u.email,u.email_verified_at,u.created_at, u.updated_at,u.apellido,u.usuario,u.descripcion
        from usuarios_grupos ug left join users u on ug.id_usuario=u.id where ug.id_grupo = :idGrupo', ["idGrupo" => $idGrupo]);
    }


    //Obtener todos los grupos de un usuario

    public static function obtenerGruposDeUsuario($idUsuario)
    {
        return DB::select('select g.* from usuarios_grupos ug left join grupos g on ug.id_grupo=g.id_grupo where ug.id_usuario = :idUsuario', ["idUsuario" => $idUsuario]);
    }





    /**
     *  ************************** ADMINISTRACION DE USUARIOS *****************************
     * 
     */


    
    //Elimina cualquier asociacion del usuario con grupos y otras tablas que puedan existir.

    public static function borrarUsuarioGrupo($usuarioId)
    {
        $result = DB::delete('delete from usuarios_grupos where id_usuario = :usuarioId', ["usuarioId" => $usuarioId]);
        return $result;
    }


    //Agrega una ruta a un grupo.

    public static function crearGrupoRuta($datos, $idGrupo, $idRuta)
    {
        $result = DB::insert(
            'insert into grupos_rutas_metodos(id_grupo,id_ruta,metodo_acceso,flag_habilitado, descripcion) values (:idGrupo, :idRuta, :metodoAcceso, :flagHabilitado, :descripcion)',
            ["idGrupo" => $idGrupo, "idRuta" => $idRuta, "metodoAcceso" => $datos["metodoAcceso"], "flagHabilitado" => $datos["flagHabilitado"], "descripcion" => $datos["descripcion"]]
        );
        return $result;
    }


    //Elimina una ruta de un grupo.

    public static function eliminarGrupoRuta($idGrupo, $idRuta, $metodoAcceso)
    {
        $result = DB::delete('delete from grupos_rutas_metodos where id_Grupo = :idGrupo and id_ruta = :idRuta and metodo_acceso = :metodoAcceso', ["idGrupo" => $idGrupo, "idRuta" => $idRuta, "metodoAcceso" => $metodoAcceso ]);
        return $result;
    }


    //Obtener rutas asignadas a grupos. Devuelve todas las rutas asociadas a los grupos creados.

    public static function obtenerGruposRutas()
    {
        return DB::select('select r.ruta, gr.nombre_grupo, grm.id_grupo, grm.metodo_acceso, grm.flag_habilitado, grm.descripcion  FROM rutas as r join grupos as gr join grupos_rutas_metodos as grm where r.id_ruta = grm.id_ruta and grm.id_grupo = gr.id_grupo');
        
    }


    //Obtener todas las rutas asigandas a un grupo en especifico

    public static function obtenerRutasDeGrupo($idGrupo)
    {
        
        return DB::select('select grm.id_gruporutametodo, r.ruta, grm.metodo_acceso, grm.descripcion from grupos_rutas_metodos grm left join rutas r on grm.id_ruta=r.id_ruta where grm.id_grupo = :idGrupo', ["idGrupo" => $idGrupo]);
    }


    //Obtener todos los grupos asignados a una ruta especifica

    public static function obtenerGruposDeRuta($idRuta)
    {

        return DB::select('select grm.metodo_acceso, g.id_grupo, g.nombre_grupo, grm.descripcion from grupos_rutas_metodos grm left join grupos g on grm.id_grupo = g.id_grupo where grm.id_ruta = :idRuta', ["idRuta" => $idRuta]);
    }
}
