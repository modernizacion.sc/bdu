<?php

namespace App\Http\Controllers;

use Exception;
use Validator;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Mail\recuperarContraseña;
use App\Repository\AdminRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\TokenRepository;
use Laravel\Passport\Client as OClient; 
use GuzzleHttp\Exception\ClientException;
use Laravel\Passport\RefreshTokenRepository;

class AdminController extends Controller
{

    /*
     **************************** USERS ******************************
     */



    public $successStatus = 200;

    /*
    La funcion login permite que un usuario se loguee si la credenciales son correctas y ademas si el usuario esta activo,
    es decir, que no haya sido deshabilitado. En ese caso, no podra realizar ninguna accion.
    */

    public function login() {

        $usuario = User::where('usuario', '=', request('usuario'))->first();
        
        if (Auth::attempt(['usuario' => request('usuario'), 'password' => request('password')]) && $usuario->esta_activo == 1) { 
            $oClient = OClient::where('password_client', 1)->first();
            return $this->getTokenAndRefreshToken($oClient, request('usuario'), request('password'));
        } 
        else { 
            return response()->json(['error'=>'Credenciales invalidas'], 401); 
        } 
    }



    /**
     * Permite registrar un usuario en el sistema
     */

    public function register(Request $request) {

        $validator = Validator::make($request->all(), [ 
            'nombre' => 'required',
            'apellido' => 'required',
            'usuario' => 'required|unique:users', 
            'email' => ['required', 'string', 'email','regex:/(.*)@(.*)\.(.*)$/i', 'max:255', 'unique:users'],
            'password' => 'required|min:6', 
            'descripcion' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all(); 

        if(!(AdminController::validar_clave($input["password"], $error_clave)))
        {
            return response()->json($error_clave, 404);
        }

        $password = $request->password;
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input);

        return response()->json("Se ha registrado correctamente", 200);            
    }



    /**
     * Retorna un access token y un refresh token al loguearse
     */

    public function getTokenAndRefreshToken(OClient $oClient, $usuario, $password) { 
        
        $http = new Client;
        $response = $http->request('POST', 'http://localhost/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'username' => $usuario,
                'password' => $password,
                'scope' => '*',
            ],
            
        ]);

        $result = json_decode((string) $response->getBody(), true);
        return response()->json($result, $this->successStatus);
    }



    /**
     * Refresca un token que ha expirado su tiempo de uso
     */
    
    public function RefreshToken(Request $request) { 
        
    $oClient = OClient::where('password_client', 1)->first();
    $http = new Client;
    try{
    $response = $http->request('POST', 'http://localhost/oauth/token', [
        'form_params' => [
        'grant_type' => 'refresh_token',
        'refresh_token' =>  $request->bearerToken(),
        'client_id' => $oClient->id,
        'client_secret' => $oClient->secret,
        'scope' => '*',
        ],
      ]);
      $result = json_decode((string) $response->getBody(), true);
      return response()->json($result, $this->successStatus);
    }
    
    catch(\Exception $e ){
        $v = $e->getMessage();
        return response()->json($e->getMessage(),401);
    }
    }




    /*
    Realiza el logout del usuario actual (que se encuentra logueado), revocando todos los access token y refresh token del usuario.
    No los elimina de la BD, pero setea el campo revoke a 1, el cual los deja inutilizables.
    */

    public function cerrarSesiones(Request $request)
    {
        $user=$request->user();
        $tokens = $user->tokens()->get(); //Obtenemos un array con todos los access token del usuario
        $tokenRepository = app(TokenRepository::class);
        $refreshTokenRepository = app(RefreshTokenRepository::class);

        foreach($tokens as $t){

            $tokenId = $t->id;

            if(!$tokenRepository->isAccessTokenRevoked($tokenId)){
                // Revoca todos los access token
                $tokenRepository->revokeAccessToken($tokenId);

                // Revoca todos los refresh token del access token
                $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($tokenId);
            }
        }
        return response()->json("Su sesion a finalizado");
    }



    /*
    Realiza el logout de un usuario en particular, revocando todos los access token y refresh token del usuario.
    No los elimina de la BD, pero setea el campo revoke a 1, el cual los deja inutilizables.
    */

    public function logoutss(User $user)
    {
        $tokens = $user->tokens()->get(); //Obtenemos un array con todos los access token del usuario
        $tokenRepository = app(TokenRepository::class);
        $refreshTokenRepository = app(RefreshTokenRepository::class);

        foreach($tokens as $t){

            $tokenId = $t->id;

            if(!$tokenRepository->isAccessTokenRevoked($tokenId)){
                // Revoca todos los access token
                $tokenRepository->revokeAccessToken($tokenId);

                // Revoca todos los refresh token del access token
                $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($tokenId);
            }
        }
        return response()->json("Su sesion a finalizado");
    }



    /*
    Realiza el logout del usuario actual (que se encuentra logueado), revocando el access token que se esta utilizando y su refresh token. (No los elimina de la BD)
    Solamente setea el campo revoked de las tablas oauth_access_tokens y oauth_refresh_tokens a 1.
    */

    public function cerrarSesionActual(Request $request)
    {
        $user=$request->user();
        $token = $user->token(); //Obtenemos un array con todos los access token del usuario
        $tokenRepository = app(TokenRepository::class);
        $refreshTokenRepository = app(RefreshTokenRepository::class);

        $tokenId = $token->id;
        if(!$tokenRepository->isAccessTokenRevoked($tokenId)){

            // Revoca todos los access token
            $tokenRepository->revokeAccessToken($tokenId);

            // Revoca todos los refresh token del access token
            $refreshTokenRepository->revokeRefreshTokensByAccessTokenId($tokenId);

            return response()->json("Su sesion a finalizado");
        }
    }
       
    


    /**
     * Retorna todos los usuarios registrados
     */

    public function listarUsuarios()
    {
        try{
            
            $usuarios = User::all();
            
            if($usuarios != '[]'){
            return response()->json($usuarios);
            }
            else{
                return response()->json("No existen usuarios cargados.", 404);
            }

        } 
        catch (Exception $e) {

            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite al usuario actual logueado (sesion actual) actualizar su password
     * Recibe en el body del request los datos del usuario.
     * passwordAnterior, nuevaPassword, confirmacionNuevaPassword
     */

    public function actualizarMiPassword(Request $request)
    {
        $usuario = $request->user();
        Log::info("Actualizar password de " . $usuario->usuario);
        $datos = $request->all();
        if( Hash::check($datos["password"], $usuario->password)){
            if(AdminController::validar_clave($datos["nuevo_password"], $error_clave)){

                $nuevo_password = Hash::make($datos["nuevo_password"]);
                $usuario->password = $nuevo_password;
                $usuario->save();
                return response()->json("Contraseña de ".$usuario->usuario." actualizada", 201);
        
            }
            else
            {
                return response()->json($error_clave, 404);
            }
        
        }
        else {
            return response()->json("No se pudo actualizar la contraseña de ".$usuario->usuario.". Contraseña incorrecta.", 400);
        }
        
    }



    /**
     * Obtiene los datos del usuario actual logueado (sesion actual) 
     */
    
    public function obtenerMisDatos(Request $request)
    {
        try {
            
            $usuario = $request->user();
            
            $usuario = User::find($usuario->id);

            return response()->json($usuario);

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite al usuario Administrador actualizar el password de otro usuario.
     */

    public function cambiarPasswordUsers($id_usuario, Request $request)
    {
        $datos = $request->password;
        
        $usuario = User::find($id_usuario);
        
        if(!empty($usuario)){

            Log::info("Actualizar password de " . $usuario->usuario);
        
            $nuevo_password = Hash::make($datos);
            
            $usuario->password = $nuevo_password;
            $usuario->save();
            return response()->json("Contraseña de ".$usuario->usuario." actualizada", 201);
        }
        else{

            return response()->json("El usuario con el Id: ".$request->id." no existe.", 404);
        }
    }



    /**
     * Permite al usuario Administrador desactivar a un usuario y lo desloguea inmediatamente para que no siga utilizando el api 
     */

    public function desactivarUsuario(Request $request)
    {
        $usuario = User::find($request->id_usuario);
        
        if(!empty($usuario)){
            if($usuario->esta_activo == 1){

                Log::info("usuario desactivado:" . $usuario->usuario);
                $usuario->esta_activo = 0;
                $usuario->save();
                AdminController::logoutss(User::find($request->id_usuario));
                return response()->json("Usuario ".$usuario->usuario." deshabilitado", 201);
            }
            else{
                return response()->json("Usuario ".$usuario->usuario." ya se encuentra deshabilitado", 201);
            }
        }
        else{
            return response()->json("El usuario con el Id: ".$request->id." no existe.", 404);
        }
    }



    /**
     * Permite al usuario Administrador activar a un usuario para que siga utilizando el api 
     */

    public function activarUsuario(Request $request)
    {
        
        $usuario = User::find($request->id_usuario);
        
        if(!empty($usuario)){
            if($usuario->esta_activo == 0){

                Log::info("usuario desactivado:" . $usuario->usuario);
                $usuario->esta_activo = 1;
                $usuario->save();
                return response()->json("Usuario ".$usuario->usuario." habilitado", 201);
            }
            else{
                return response()->json("Usuario ".$usuario->usuario." ya se encuentra habilitado", 201);
            }
        }
        else{
            return response()->json("El usuario con el Id: ".$request->id." no existe.", 404);
        }
    }



    /**
     * Da de alta un usuario.
     * Recibe en el body del request los datos del usuario a crear.
     */

    public function creaUsuario(Request $request)
    {
        try {
            $datos = $request->all();
            
            Log::debug("Crear usuario: " . print_r($datos, true));
        
            //Consultamos en la BD si el email que introducimos en el body ya existe
            $emailUsuario = User::where('email', '=', $datos["email"])->first();

            //Consultamos en la BD si el nombre de usuario que introducimos en el body ya existe
            $nombreUsuario = User::where('usuario', '=', $datos["usuario"])->first();
            
            //Validamos, si el email no existe en la BD, validamos el nombre de usuario. 
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(empty($emailUsuario)){
                if(filter_var($datos["email"], FILTER_VALIDATE_EMAIL)){
                    //si el nombre de usuario tampoco existe en la BD, se crea el usuario ya que ninguno
                    //de los campos unique se repiten.
                    if(empty($nombreUsuario)){
                        if(preg_match("/^[a-zA-Z ]*$/",$datos["apellido"]) && preg_match("/^[a-zA-Z ]*$/",$datos["nombre"] ))
                        {
                            if(AdminController::validar_clave($datos["password"], $error_clave)){
                            // codifica la password
                            $datos["password"] = Hash::make($datos["password"]);
                            $usuario = new User();
                            $usuario->create($datos);
                            return response()->json("Usuario creado con éxito");
                            
                            }
                        else{
                            return response()->json($error_clave, 404);
                        }
                    }
                        else{
                            return response()->json("Solo letras o espacios en blanco para Nombre y Apellido", 404);
                        }
                    }
                    else{
                        return response()->json("El nombre de usuario ya existe", 404);
                    }
                }
                else{
                    return response()->json("Formato de email incorrecto", 404);
                }
            }
            else{
                return response()->json("El email ya existe", 404);
            }
        
        }
        //Captura la excepcion lanzada al intentar dar de alta un usuario, con un nombre de usuario y/o email que ya se encuentra en la BD.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El nombre de usuario y/o email del usuario que intenta crear ya existe.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite actualizar los datos del usuario actual logueado (sesion actual)
     */

    public function actualizarMisDatos(Request $request)
    {
        try {

            //Obtenemos los datos del usuario actual (con el que me encuentro logueado)
            $usuario = $request->user();
            
            //Obtenemos los datos del body
            $datos = $request->all();
        
            //Consultamos en la BD si el email que introducimos en el body ya existe
            $emailUsuario = User::where('email', '=', $datos["email"])->first();
                        
            //Consultamos en la BD si el nombre de usuario que introducimos en el body ya existe
            $nombreUsuario = User::where('usuario', '=', $datos["usuario"])->first();


            $validator = Validator::make($request->all(), [ 
                'nombre' => 'required',
                'apellido' => 'required',
                'usuario' => 'required', 
                'email' => ['required', 'string', 'email','regex:/(.*)@(.*)\.(.*)$/i', 'max:255'],
                'descripcion' => 'required'
            ]);
    
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }

            //Validamos, si el email no existe en la BD o el email actual es igual al que se encuentra en el body
            //(esto quiere decir que no lo quiero modificar) 
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(empty($emailUsuario) || $usuario->email == $datos["email"]){
                if(filter_var($datos["email"], FILTER_VALIDATE_EMAIL)){
                    //si el nombre de usuario tampoco existe en la BD o el usuario actual es igual al que se encuentra en el body, 
                    //(esto quiere decir que no lo quiero modificar), se modifica el usuario ya que ninguno de los campos unique se repiten.
                    if(empty($nombreUsuario) || $usuario->usuario == $datos["usuario"]){
                        if(preg_match("/^[a-zA-Z ]*$/",$datos["apellido"]) && preg_match("/^[a-zA-Z ]*$/",$datos["nombre"] ))
                        {
                            // Se pisan todos los datos del usuario.
                            // Si el dato no cambio, debe venir igual con el valor previo. De lo contrario hay que verificar si el dato
                            // exista, realizar la asignacion.
                            Log::info("Actualiza usuario ID: " . $request->id . " - " . $usuario);
                            $usuario->nombre = $datos["nombre"];
                            $usuario->apellido = $datos["apellido"];
                            $usuario->usuario = $datos["usuario"];
                            $usuario->descripcion = $datos["descripcion"];
                            $usuario->email = $datos["email"];
                            $usuario->save();
                            return response()->json("Usuario modificado con éxito", 201);
                        }
                        else{
                            return response()->json("Solo letras o espacios en blanco para Nombre y Apellido", 404);
                        }
                    }
                    else{
                        return response()->json("El nombre de usuario ya existe", 404);
                    }
            }
            else{
                return response()->json("Formato de email incorrecto", 404);

            }
        }
        else{
            return response()->json("El email ya existe", 404);
        }
            
        } 
        //Captura la excepcion lanzada al intentar modificar un usuario, con un nombre de usuario y/o email que ya se encuentra en la BD, distinto al que posee.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El nombre de usuario y/o email del usuario que intenta crear ya existe.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }




    /**
     * Permite actualizar el nombre del usuario actual logueado (sesion actual)
     */

    public function actualizarMiNombre(Request $request)
    {
        try {

            //Obtenemos los datos del usuario actual (con el que me encuentro logueado)
            $usuario = $request->user();
            
            //Obtenemos los datos del body
            $datos = $request->all();
            
        
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($datos["nombre"])){
                 
                        if(preg_match("/^[a-zA-Z ]*$/",$datos["nombre"]))
                        {
                            Log::info("Actualiza usuario ID: " . $request->id . " - " . $usuario);
                            $usuario->nombre = $datos["nombre"];
                            $usuario->save();
                            return response()->json("El nombre del usuario ha sido modificado con éxito", 201);
                        }
                        else{
                            return response()->json("Solo letras o espacios en blanco para Nombre", 404);
                        }
        }
        else{
            return response()->json("El nombre no puede estar vacio", 404);
        }
            
        } 
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El nombre de usuario no puede estar vacio",404);
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite actualizar el email del usuario actual logueado (sesion actual)
     */ 

    public function actualizarMiEmail(Request $request)
    {
        try {

            //Obtenemos los datos del usuario actual (con el que me encuentro logueado)
            $usuario = $request->user();
            
            //Obtenemos los datos del body
            $datos = $request->all();
        
            //Consultamos en la BD si el email que introducimos en el body ya existe
            $emailUsuario = User::where('email', '=', $datos["email"])->first();

            //Validamos, si el email no existe en la BD o el email actual es igual al que se encuentra en el body
            //(esto quiere decir que no lo quiero modificar) 
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(empty($emailUsuario) || $usuario->email == $datos["email"]){
                if(!empty($datos["email"])){
                    if(filter_var($datos["email"], FILTER_VALIDATE_EMAIL)){
                    
                        Log::info("Actualiza usuario ID: " . $request->id . " - " . $usuario);
                        
                        $usuario->email = $datos["email"];
                        $usuario->save();
                        return response()->json("El email del usuario ha sido modificado con éxito", 201);
                    }
                    else{
                        return response()->json("Formato de email incorrecto", 404);
                    }
                }
                else{
                    return response()->json("El email no puede estar vacio", 404);
                }
                        
            }
            else{
                return response()->json("El email ya existe", 404);
            }
        }
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El email del usuario que intenta crear ya existe.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite actualizar el apellido del usuario actual logueado (sesion actual)
     */

    public function actualizarMiApellido(Request $request)
    {
        try {

            //Obtenemos los datos del usuario actual (con el que me encuentro logueado)
            $usuario = $request->user();
            
            //Obtenemos los datos del body
            $datos = $request->all();
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($datos["apellido"])){
                 
                if(preg_match("/^[a-zA-Z ]*$/",$datos["apellido"]))
                {
                    Log::info("Actualiza usuario ID: " . $request->id . " - " . $usuario);
                    $usuario->apellido = $datos["apellido"];
                    $usuario->save();
                    return response()->json("El apellido del usuario ha sido modificado con éxito", 201);
                }
                else{
                        return response()->json("Solo letras o espacios en blanco para apellido", 404);
                    }
            }
            else{
                return response()->json("El apellido no puede estar vacio", 404);
            }
            
        } 
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El apellido no puede estar vacio",404);
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite actualizar el nombre de usuario del usuario actual logueado (sesion actual)
     */

    public function actualizarMiNombreUsuario(Request $request)
    {
        try {

            //Obtenemos los datos del usuario actual (con el que me encuentro logueado)
            $usuario = $request->user();
            
            //Obtenemos los datos del body
            $datos = $request->all();
                        
            //Consultamos en la BD si el nombre de usuario que introducimos en el body ya existe
            $nombreUsuario = User::where('usuario', '=', $datos["usuario"])->first();

            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($datos["usuario"])){

                //si el nombre de usuario no existe en la BD o el usuario actual es igual al que se encuentra en el body, 
                //(esto quiere decir que no lo quiero modificar), se modifica el usuario ya que ninguno de los campos unique se repiten.
                if(empty($nombreUsuario) || $usuario->usuario == $datos["usuario"]){
                        
                    Log::info("Actualiza usuario ID: " . $request->id . " - " . $usuario);
                            
                    $usuario->usuario = $datos["usuario"];  
                    $usuario->save();
                    return response()->json("El nombre de usuario se ha modificado con éxito", 201);   
                }
                else{
                    return response()->json("El nombre de usuario ya existe", 404);
                }
            }
            else{
                return response()->json("El nombre de usuario no puede estar vacio", 404);
            }
            
        } 
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El nombre de usuario del usuario que intenta modificar ya existe.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite actualizar la descripcion del usuario actual logueado (sesion actual)
     */

    public function actualizarMiDescripcion(Request $request)
    {
        try {

            //Obtenemos los datos del usuario actual (con el que me encuentro logueado)
            $usuario = $request->user();
            
            //Obtenemos los datos del body
            $datos = $request->all();
             
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($datos["descripcion"])){
               
                Log::info("Actualiza usuario ID: " . $request->id . " - " . $usuario);
                $usuario->descripcion = $datos["descripcion"];
                $usuario->save();
                return response()->json("La descripcion del usuario ha sido modificada con éxito", 201);
                       
            }
            else{
                return response()->json("La descripcion no puede estar vacia", 404);
            }
            
        } 
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql",404);
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite al usuario Administrador actualizar todos los datos de un usuario.
     */

    public function actualizaUsuario(Request $request, $id)
    {
        try {

            //Obtenemos los datos del body
            $datos = $request->all();

            //Consultamos en la BD si el usuario existe
            $usuario = User::find($id);

            //Consultamos en la BD si el email que introducimos en el body ya existe
            $emailUsuario = User::where('email', '=', $datos["email"])->first();
                        
            //Consultamos en la BD si el nombre de usuario que introducimos en el body ya existe
            $nombreUsuario = User::where('usuario', '=', $datos["usuario"])->first();


            $validator = Validator::make($request->all(), [ 
                'nombre' => 'required',
                'apellido' => 'required',
                'usuario' => 'required', 
                'email' => ['required', 'string', 'email','regex:/(.*)@(.*)\.(.*)$/i', 'max:255'],
                'descripcion' => 'required'
            ]);
    
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }

            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){

            //Validamos, si el email no existe en la BD o el email actual es igual al que se encuentra en el body
            //(esto quiere decir que no lo quiero modificar) 
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
                if(empty($emailUsuario) || $usuario->email == $datos["email"]){
                    if(filter_var($datos["email"], FILTER_VALIDATE_EMAIL)){
                //si el nombre de usuario tampoco existe en la BD o el usuario actual es igual al que se encuentra en el body, 
                //(esto quiere decir que no lo quiero modificar), se modifica el usuario ya que ninguno de los campos unique se repiten.
                        if(empty($nombreUsuario) || $usuario->usuario == $datos["usuario"]){
                            if(preg_match("/^[a-zA-Z ]*$/",$datos["apellido"]) && preg_match("/^[a-zA-Z ]*$/",$datos["nombre"] ))
                            {
                                // No se actualiza el password
                                // Se pisan todos los datos del usuario.
                                // Si el dato no cambio, debe venir igual con el valor previo. De lo contrario hay que verificar si el dato
                                // exista, realizar la asignacion.
                                Log::info("Actualiza usuario ID: " . $id . " - " . $usuario);
                                $usuario->nombre = $datos["nombre"];
                                $usuario->apellido = $datos["apellido"];
                                $usuario->usuario = $datos["usuario"];
                                $usuario->descripcion = $datos["descripcion"];
                                $usuario->email = $datos["email"];
                                $usuario->save();
                                return response()->json("Usuario modificado con éxito", 201);
                            }
                            else{
                                return response()->json("Solo letras o espacios en blanco para Nombre y Apellido", 404);
                            }
                        
                        }
                        else{
                            return response()->json("El nombre de usuario ya existe", 404);
                        }
                    }
                    else{
                        return response()->json("Formato de email incorrecto", 404);
                    }
                }
                else{
                    return response()->json("El email ya existe", 404);
                }
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } 
        //Captura la excepcion lanzada al intentar modificar un usuario, con un nombre de usuario y/o email que ya se encuentra en la BD, distinto al que posee.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El nombre de usuario y/o email del usuario que intenta crear ya existe.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }




    /**
     * Permite al usuario Administrador actualizar el nombre de un usuario.
     */

    public function actualizarNombreUsuario(Request $request, $id)
    {
        try {

            //Obtenemos los datos del body
            $datos = $request->all();

            //Consultamos en la BD si el usuario existe
            $usuario = User::find($id);

            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){

                //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
                if(!empty($datos["nombre"])){

                    if(preg_match("/^[a-zA-Z ]*$/",$datos["nombre"]))
                    {
                    
                        Log::info("Actualiza usuario ID: " . $id . " - " . $usuario);
                        $usuario->nombre = $datos["nombre"];
                        $usuario->save();
                        return response()->json("El nombre del usuario se ha modificado con éxito", 201);
                    }
                    else{
                        return response()->json("Solo letras o espacios en blanco para el Nombre", 404);
                    }
                }else{
                    return response()->json("El nombre no puede estar vacio", 404);
                }
                        
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } 
    
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite al usuario Administrador actualizar el email de un usuario.
     */

    public function actualizarEmailUsuario(Request $request, $id)
    {
        try {

            //Obtenemos los datos del body
            $datos = $request->all();

            //Consultamos en la BD si el usuario existe
            $usuario = User::find($id);

            //Consultamos en la BD si el email que introducimos en el body ya existe
            $emailUsuario = User::where('email', '=', $datos["email"])->first();

            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){

                //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
                if(!empty($datos["email"])){
                    //Validamos, si el email no existe en la BD o el email actual es igual al que se encuentra en el body
                    //(esto quiere decir que no lo quiero modificar) 
                    //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
                    if(empty($emailUsuario) || $usuario->email == $datos["email"]){
                        if(filter_var($datos["email"], FILTER_VALIDATE_EMAIL)){
                    
                            Log::info("Actualiza usuario ID: " . $id . " - " . $usuario);
                            $usuario->email = $datos["email"];
                            $usuario->save();
                            return response()->json("El email del usuario se ha modificado con éxito", 201);
                        }
                        else{
                            return response()->json("Formato de email incorrecto", 404);
                        }
                    }
                    else{
                        return response()->json("El email ya existe", 404);
                    }
                }
                else{
                    return response()->json("El email no puede estar vacio", 404);
                }
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } 
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El email del usuario que intenta modificar ya existe.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite al usuario Administrador actualizar el apellido de un usuario.
     */

    public function actualizarApellidoUsuario(Request $request, $id)
    {
        try {

            //Obtenemos los datos del body
            $datos = $request->all();

            //Consultamos en la BD si el usuario existe
            $usuario = User::find($id);

            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){

                //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
                if(!empty($datos["apellido"])){

                    if(preg_match("/^[a-zA-Z ]*$/",$datos["apellido"]))
                    {
                    
                        Log::info("Actualiza usuario ID: " . $id . " - " . $usuario);
                        $usuario->apellido = $datos["apellido"];
                        $usuario->save();
                        return response()->json("El apellido del usuario se ha modificado con éxito", 201);
                    }
                    else{
                        return response()->json("Solo letras o espacios en blanco para el Apellido", 404);
                    }
                }else{
                    return response()->json("El Apellido no puede estar vacio", 404);
                }
                        
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } 
    
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite al usuario Administrador actualizar el nombre de usuario de un usuario.
     */

    public function actualizarUserNameUsuario(Request $request, $id)
    {
        try {

            //Obtenemos los datos del body
            $datos = $request->all();

            //Consultamos en la BD si el usuario existe
            $usuario = User::find($id);
                        
            //Consultamos en la BD si el nombre de usuario que introducimos en el body ya existe
            $nombreUsuario = User::where('usuario', '=', $datos["usuario"])->first();

            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){

                //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
                if(!empty($datos["usuario"])){
                        //si el nombre de usuario no existe en la BD o el usuario actual es igual al que se encuentra en el body, 
                        //(esto quiere decir que no lo quiero modificar), se modifica el usuario ya que ninguno de los campos unique se repiten.
                        if(empty($nombreUsuario) || $usuario->usuario == $datos["usuario"]){
                           
                                Log::info("Actualiza usuario ID: " . $id . " - " . $usuario);
                                $usuario->usuario = $datos["usuario"];
                                $usuario->save();
                                return response()->json("El nombre de Usuario se ha modificado con éxito", 201);
                        
                        }
                        else{
                            return response()->json("El nombre de usuario ya existe", 404);
                        }
                }
                else{
                    return response()->json("El nombre de usuario no puede estar vacio", 404);
                }
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } 
        //Captura la excepcion lanzada al intentar modificar un usuario, con un nombre de usuario y/o email que ya se encuentra en la BD, distinto al que posee.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El nombre de usuario y/o email del usuario que intenta crear ya existe.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Permite al usuario Administrador actualizar la descripcion de un usuario.
     */

    public function actualizarDescripcionUsuario(Request $request, $id)
    {
        try {

            //Obtenemos los datos del body
            $datos = $request->all();

            //Consultamos en la BD si el usuario existe
            $usuario = User::find($id);

            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){

                //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
                if(!empty($datos["descripcion"])){
                    
                    Log::info("Actualiza usuario ID: " . $id . " - " . $usuario);
                    $usuario->descripcion = $datos["descripcion"];
                    $usuario->save();
                    return response()->json("La descripcion del usuario se ha modificado con éxito", 201);
                  
                }else{
                    return response()->json("La descripcion no puede estar vacia", 404);
                }
                        
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } 
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }




    /**
     * Elimina un usuario y sus asociaciones (grupos a los que pertenece)
     */

    public function borrarUsuario($id)
    {
        try {

            $usuario = User::find($id);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){
                // Borra las asociaciones (grupos a los que pertenece) que existan con ese usuario
                AdminRepository::borrarUsuarioGrupo($id);
                //Borra al usuario
                $usuario->delete();
                return response()->json("Usuario borrado");
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Retorna los datos de un usuario, sin asociaciones, por Id
     */

    public function obtenerUsuarioId($id)
    {
        try {
            $usuario = User::find($id);

            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){
                return response()->json($usuario);
            }
            else{
                return response()->json("No existe ningun usuario con el id: ".$id, 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Retorna los datos de un usuario, por email
     */

    public function obtenerUsuarioEmail($email)
    {
        try {
            $usuario = User::where('email', '=', $email)->first();
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($usuario)){
                return response()->json($usuario);
            }
            else{
                return response()->json("No existe ningun usuario con el email: ".$email, 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Retorna los datos de los usuarios, por nombre
     */

    public function obtenerUsuarioNombre($nombre)
    {
        try {
            $usuario = User::where('nombre', '=', $nombre)->get();
            
            if($usuario != '[]'){
                return response()->json($usuario);
            }
            else{
                return response()->json("No existe ningun usuario con el nombre: ".$nombre, 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Retorna los datos de un usuario, por apellido
     */

    public function obtenerUsuarioApellido($apellido)
    {
        try {
            $usuario = User::where('apellido', '=', $apellido)->get();

            if($usuario != '[]'){
                return response()->json($usuario);
            }
            else{
                return response()->json("No existe ningun usuario con el apellido: ".$apellido, 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /*
     **************************** GRUPOS ******************************
     */


    /**
     * Crea un nuevo grupo
     */
    public function crearGrupo(Request $request)
    {
        try {
            
            $datos = $request->all();

            //Consultamos en la BD si el nombre del grupo que introducimos en el body ya existe
            $nombreGrupo = AdminRepository::obtenerGrupoNombre($datos["nombre"]);

            if(empty($nombreGrupo)){

                Log::debug("Creando grupo: " . print_r($datos, true));
                AdminRepository::crearGrupo($datos);
                return response()->json("Grupo creado con éxito");
            }
            else{
                return response()->json("El nombre del grupo que intenta crear ya existe", 404);
            }
            
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }

    

    /**
     * Actualiza los datos de un grupo.
     */

    public function actualizarGrupo(Request $request, $id)
    {
        try {
            $datos = $request->all();
            Log::debug("Actualizando grupo: " . print_r($datos, true));

            //Consultamos en la BD si el ID del grupo que queremos modificar existe
            $grupo = AdminRepository::obtenerGrupo($id);
        
            //Consultamos en la BD si el nombre del grupo que introducimos en el body ya existe en cualquier otro grupo, distinto al que queremos modificar.
            $nombre = AdminRepository::obtenerGrupoNombre($datos["nombre_grupo"]);
            
            //Si $grupo no esta vacio, quiere decir que el grupo existe.
            if(!empty($grupo)){
                
                //Si el nombre no se encuentra en la BD o el nombre que enviamos en el body es igual al que ya tiene el grupo a modificar, actualizamos.
                if(empty($nombre) || $datos["nombre_grupo"] == $grupo[0]->nombre_grupo){

                    AdminRepository::actualizarGrupo($datos, $id);
                    return response()->json("Grupo actualizado", 201);    
                }
                else{
                    return response()->json("El nombre del grupo ya existe.");    
                }
            }
            else{
                return response()->json("El grupo con el Id: " . $id . " no existe.");
            }
            
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Elimina un grupo.
     * TODO verificar si tiene registros de otras tablas vinculados, o si fallaría por integridad referencial
     */

    public function borrarGrupo($idGrupo)
    {
        try {

            $grupo = AdminRepository::obtenerGrupo($idGrupo);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($grupo)){
            
                $resultado = AdminRepository::borrarGrupo($idGrupo);

                if($resultado == 1){
                    return response()->json("Grupo borrado");
                }else{
                    return response()->json("No se ha podido borrar el grupo", 404);
                }

            }
            else{
                return response()->json("No existe ningun grupo con el id: ".$idGrupo, 404);
            }

        } 
        //Captura la excepcion lanzada al intentar borrar un grupo que tenga usuarios y/o rutas asociados.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. El grupo que desea eliminar tiene usuarios y/o rutas asociados.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Lista todos los grupos
     */

    public function listarGrupos()
    {
        try {
            return AdminRepository::listarGrupos();
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Obtiene un grupo, por su Id
     */

    public function obtenerGrupo($id)
    {
        try {
            $grupo = AdminRepository::obtenerGrupo($id);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($grupo)){
                return response()->json($grupo);
            }
            else{
                return response()->json("No existe ningun grupo con el id: ".$id, 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



     /**
     * Obtiene un grupo, por su nombre
     */

    public function obtenerGrupoNombre($nombreGrupo)
    {
        try {
            $grupo = AdminRepository::obtenerGrupoNombre($nombreGrupo);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($grupo)){
                return response()->json($grupo);
            }
            else{
                return response()->json("No existe ningun grupo con el nombre: ".$nombreGrupo, 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }
     
    


    /*
     **************************** RUTAS ******************************
     */



    /**
     * Crea una ruta
     */

    public function crearRuta(Request $request)
    {
            
        try {
            $datos = $request->all();

            Log::debug("Creando ruta: " . print_r($datos, true));
            AdminRepository::crearRuta($datos);
            return response()->json("Ruta creada con éxito");
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }

    

    /**
     * Actualiza los datos de una ruta
     */

    public function actualizarRuta(Request $request, $id)
    {
        try {

            $ruta = AdminRepository::obtenerRuta($id);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($ruta)){

                $datos = $request->all();

                Log::debug("Actualizando ruta: " . print_r($datos, true));

                $resultado = AdminRepository::actualizarRuta($datos, $id);

                if($resultado == 1){

                    return response()->json("Ruta actualizada", 201);

                }
                else{
                    return response()->json("No se ha podido actualizar la ruta", 404);
                }

            }
            else{
                return response()->json("No existe ninguna ruta con el id: ".$id, 404);
            }
            
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Elimina un ruta por metodo de la tabla grupos_rutas_metodos, segun la PK de la tabla.
     */

    public function borrarRutaMetodo($idRuta)
    {
        try {
            $resultado = AdminRepository::borrarRutaMetodo($idRuta);
            
            if($resultado == 1){
                return response()->json("ruta borrada");
            }else{
                return response()->json("No se ha podido borrar la ruta", 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Elimina un ruta por metodo de la tabla grupos_rutas_metodos, segun el id_grupo, id_ruta y el metodo_acceso.
     */

    public function borrarRutaMetodo1(REQUEST $request)
    {
        try {
            
            $idGrupo = $request->id_grupo;
            $idRuta = $request->id_ruta;
            $metodoAcceso = $request->metodo_acceso;

            $resultado = AdminRepository::borrarRutaMetodo1($idGrupo, $idRuta, $metodoAcceso);

            if($resultado == 1){
                return response()->json("ruta borrada");
            }else{
                return response()->json("No se ha podido borrar la ruta", 404);
            }
            
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Elimina un ruta generica de la tabla rutas
     * TODO verificar si tiene registros de otras tablas vinculados, o si fallaría por integridad referencial
     */

    public function borrarRuta($idRuta)
    {
        try {
            
            $ruta = AdminRepository::obtenerRuta($idRuta);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($ruta)){

                $resultado = AdminRepository::borrarRuta($idRuta);

                if($resultado == 1){

                    return response()->json("ruta borrada");

                }else{

                    return response()->view('errors.404',['message'   => 'No se ha podido borrar la ruta'], 404);
                }
            }
            else{
                return response()->json("No existe ninguna ruta con el id: ".$idRuta, 404);
            }

        } 
        //Captura la excepcion lanzada al intentar borrar una ruta que tenga grupos_rutas_metodos asociados.
        catch(\Illuminate\Database\QueryException $e){
            return response()->json("Error de integridad mysql. La ruta que desea eliminar tiene grupos_rutas_metodos asociados.");
        }
        catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }

    /**
     * Lista todas las rutas
     */

    public function listarRutas()
    {
        try {
            return AdminRepository::listarRutas();
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Obtiene una ruta segun su ID.
     */

    public function obtenerRuta($idRuta)
    {
        try {

            $ruta = AdminRepository::obtenerRuta($idRuta);
            
            //empty() devuelve 1 si una variable esta vacia, 0 caso contrario
            if(!empty($ruta)){
                return response()->json($ruta);
            }
            else{
                return response()->json("No existe ninguna ruta con el id: ".$idRuta, 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Lista las rutas que tiene accesible el usuario
     */

    public function listarRutasUsuario(Request $request)
    {
        try {
            $usuario = $request->user();
            $idUsuario = $usuario->id;
            
            $rutasUsuario = AdminRepository::listarRutasUsuario($idUsuario);
            if(!empty($rutasUsuario)){
                return response()->json($rutasUsuario);
            }
            else{
                return response()->json("No existe ninguna ruta para el usuario ".$usuario->nombre, 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }




    /**
     *************************** USUARIOS GRUPOS **************************
     */


    /*
     * Agrega un usuario a un grupo
     */    

    public function crearUsuarioGrupo($idGrupo, $idUsuario, Request $request)
    {
        try {
            //Obtenemos el flag solamente
            $datos = $request->all();

            if(AdminController::obtenerUsuarioId($idUsuario) != "[]"){

                if(!empty(AdminRepository::obtenerGrupo($idGrupo))){

                    Log::debug("Creando usuario-grupo: " . print_r($datos, true));
                    AdminRepository::crearUsuarioGrupo($idGrupo, $idUsuario, $datos);
                    return response()->json("Usuario agregado al Grupo con éxito");
                }
                else{
                    return response()->json("El Grupo no existe");
                }
            }
            else{
                return response()->json("El usuario no existe");
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /*
     * Elimina un usuario de un grupo
     */  

    public function eliminarUsuarioGrupo($idGrupo, $idUsuario)
    {
        try {

            Log::debug("Eliminando usuario-grupo: " . print_r($idUsuario, true));

            $resultado = AdminRepository::eliminarUsuarioGrupo($idUsuario, $idGrupo);

            if($resultado == 1){
                return response()->json("Usuario eliminado del Grupo con éxito");
            }
            else{
                return response()->json("El usuario no fue eliminado del Grupo", 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /**
     * Obtiene los registros de la tabla usuarios_grupos. Lista los usuarios y sus grupos.
     */

    public function obtenerUsuariosGrupos()
    {
        try {
            $resultado = AdminRepository::obtenerUsuariosGrupos();

            if(!empty($resultado)){
                return response()->json($resultado);
            }
            else{
                return response()->json("No existe ningun usuario y grupo", 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /*
     * Lista los usuarios de un grupo determinado.
     */  

    public function obtenerUsuariosDeGrupo($idGrupo)
    {
        try {
            //Primero verificamos que el grupo exista
            $grupo = AdminRepository::obtenerGrupo($idGrupo);
            //si $grupo no esta vacio, quiere decir que el grupo existe
            if(!empty($grupo)){
                //una vez validado que el grupo existe, obtenemos los usuarios del mismo.
                $resultado = AdminRepository::obtenerUsuariosDeGrupo($idGrupo);
                //validamos si el grupo posee usuarios asociados
                if(!empty($resultado)){
                    return response()->json($resultado);
                }
                else{
                    return response()->json("El grupo no cuenta con ningun usuario asociado", 404);
                }
            }
            else{
                return response()->json("El grupo no existe", 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /*
     * Lista los grupos de un usuario determinado.
     */  

    public function obtenerGruposDeUsuario($idUsuario)
    {
        try {
            //Primero verificamos que el usuario exista
            $usuario = User::find($idUsuario);
            //si $usuario no esta vacio, quiere decir que el usuario existe
            if(!empty($usuario)){
                //una vez validado que el usuario existe, obtenemos los grupos del mismo.
                $resultado = AdminRepository::obtenerGruposDeUsuario($idUsuario);
                //validamos si el usuario posee grupos asociados
                if(!empty($resultado)){
                    return response()->json($resultado);
                }
                else{
                    return response()->json("El usuario no cuenta con ningun grupo asociado", 404);
                }
            }
            else{
                return response()->json("El usuario no existe", 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }




    /**
     *************************  GRUPOS RUTAS METODOS **************************
     */


    /*
     * Agrega una ruta a un grupo, en la tabla grupos_rutas_metodos.
     */ 

    public function crearGrupoRuta($idGrupo,$idRuta, Request $request)
    {
        try {
            if(!empty(AdminRepository::obtenerGrupo($idGrupo)))
            {
                if(!empty(AdminRepository::obtenerRuta($idRuta))){
                    if( $request->metodoAcceso == "GET" ||
                        $request->metodoAcceso == "POST" ||
                        $request->metodoAcceso == "DELETE" ||
                        $request->metodoAcceso == "PUT" ||
                        $request->metodoAcceso == "PATCH")
                    {
                        
                        $datos = $request->all();

                        Log::debug("Creando grupo-ruta: " . print_r($datos, true));
                        AdminRepository::crearGrupoRuta($datos, $idGrupo, $idRuta);
                        return response()->json("Ruta agregada al Grupo con éxito");
                    }
                    else{
                        return response()->json("El metodo ingresado no es valido");

                    }
                }
                else{
                    return response()->json("La ruta no existe");
                }
            }
            else{
                return response()->json("El grupo no existe");
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /*
     * Elimina una ruta de un grupo, en la tabla grupos_rutas_metodos.
     */ 

    public function eliminarGrupoRuta($idGrupo, $idRuta, Request $request)
    {
        try {
            Log::debug("Eliminando grupo-ruta: " . print_r($idGrupo, true));
            $resultado = AdminRepository::eliminarGrupoRuta($idGrupo, $idRuta, $request->metodo_acceso);
            
            if($resultado == 1){

                return response()->json("Grupo-Ruta eliminado con éxito");
                
            }
            else{

                return response()->json("Grupo-Ruta no fue eliminado ", 404);
                
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /*
     * Devuelve todas las rutas asociadas a los grupos creados.
     */ 

    public function obtenerGruposRutas()
    {
        try {

            $resultado = AdminRepository::obtenerGruposRutas();

            if(!empty($resultado)){
                return response()->json($resultado);

            }else{
                return response()->json("No existe ningun grupo-ruta", 404);
            }
        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /*
     * Devuelve todas las rutas asociadas a un grupo especifico.
     */ 

    public function obtenerRutasDeGrupo($idGrupo)
    {
        try {

            //Primero verificamos que el grupo exista
            $grupo = AdminRepository::obtenerGrupo($idGrupo);
            //si $grupo no esta vacio, quiere decir que el grupo existe
            if(!empty($grupo)){
                //una vez validado que el grupo existe, obtenemos las rutas del mismo.
                $resultado = AdminRepository::obtenerRutasDeGrupo($idGrupo);
                //validamos si el grupo posee rutas asociadas
                if(!empty($resultado)){
                    return response()->json($resultado);
                }
                else{
                    return response()->json("El grupo no cuenta con ninguna ruta asociada", 404);
                }
            }
            else{
                return response()->json("El grupo no existe", 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }



    /*
     * Devuelve todos los grupos asociados a una ruta especifica.
     */ 

    public function obtenerGruposDeRuta($idRuta)
    {
        try {

            //Primero verificamos que la ruta exista
            $ruta = AdminRepository::obtenerRuta($idRuta);
            //si $ruta no esta vacio, quiere decir que la ruta existe
            if(!empty($ruta)){
                //una vez validado que la ruta existe, obtenemos los grupos de la misma.
                $resultado = AdminRepository::obtenerGruposDeRuta($idRuta);
                //validamos si la ruta posee grupos asociados
                if(!empty($resultado)){
                    return response()->json($resultado);
                }
                else{
                    return response()->json("La ruta no cuenta con ningun grupo asociado", 404);
                }
            }
            else{
                return response()->json("La ruta no existe", 404);
            }

        } catch (Exception $e) {
            Log::error('Error inesperado.' . $e->getMessage());
            return response()->json("Ocurrió un error al realizar la acción. " . $e->getMessage(), 500);
        }
    }


    /*
     * Valida que un password sea correcto.
     */ 
    
    public function validar_clave($clave,&$error_clave){
        if(strlen($clave) < 6){
           $error_clave = "La clave debe tener al menos 6 caracteres";
           return false;
        }
        if(strlen($clave) > 16){
           $error_clave = "La clave no puede tener más de 16 caracteres";
           return false;
        }
        if (!preg_match('`[a-z]`',$clave)){
           $error_clave = "La clave debe tener al menos una letra minuscula";
           return false;
        }
        if (!preg_match('`[A-Z]`',$clave)){
           $error_clave = "La clave debe tener al menos una letra mayuscula";
           return false;
        }
        if (!preg_match('`[0-9]`',$clave)){
           $error_clave = "La clave debe tener al menos un caracter numerico";
           return false;
        }
        $error_clave = "";
        return true;
     }
}
