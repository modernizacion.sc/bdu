<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Validator;
use App\Mail\recuperarContraseña;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ResetRequest;
use App\Http\Requests\ForgotRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ForgotController extends Controller
{
    public function forgot(ForgotRequest $request){
        
        $email = $request->input('email');

        if(User::where('email', $email)->doesntExist()){
            
            return response()->json('El usuario no existe', 404);
            
        }
        $token = Str::random(10);
        
        try{
        DB::table('password_resets')->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now(),
            'expire_at' => Carbon::now()->addMinutes(10)
        ]);

        // enviamos el Email
        $correo = new recuperarContraseña($token);
        Mail::to($request->email)->send($correo);
        
        return response()->json('Revisa tu email');
        }
        catch(\Exception $exception)
        {
            return response()->json($exception->getMessage(), 400);
        }

    }

public function reset(Request $request){
    $validator = Validator::make($request->all(), [ 
        'token' => 'required',
        'password' => 'required|min:6', 
        'password_confirm' => 'required||same:password'
    ]);

    if ($validator->fails()) { 
        return response()->json(['error'=>$validator->errors()], 401);            
    }

    if($request->password != $request->password_confirm){
        return response()->json('Las contraseñas no coinciden', 400);
    }
    $token=$request->input('token');
    if(!$passwordResets = DB::table('password_resets')->where('token', $token)->first()){
        return response()->json('Token Invalido!', 400);
    }
    if (!$user = User::where('email', $passwordResets->email)->first())
    {
    return response()->json('El usuario no existe!', 404);
    }
    if (( Carbon::now()->isAfter($passwordResets->expire_at)))
    {
    return response()->json('El token a caducado!', 404);
    }

    $nuevo_password = Hash::make($request->password);
                $user->password = $nuevo_password;
                $user->save();
                return response()->json("Contraseña de ".$user->usuario." actualizada", 201);        
}


}