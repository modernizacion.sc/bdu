<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VerificaGrupo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $grupo)
    {
        if (true) {
            Log::info('Middleware grupo: '.$grupo);
            Log::info('Usuario ?');
        }
        return $next($request);
    }
}
