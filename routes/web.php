<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ForgotController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// IMPORTANTE //

/*
    Debe viajar el HEADER Accept application/json para que las respuestas con errores sean manejadas correctamente,
    de lo contrario retorna un html con mensaje del error
*/

Route::get('/', function () {
    return view('welcome');
});
// Rutas publicas 
Route::post('/v1/login', [AdminController::class,'login']);
Route::post('/v1/recuperar', [ForgotController::class,'forgot']);
Route::post('/v1/restablecimiento', [ForgotController::class,'reset']);
Route::post('/v1/register', [AdminController::class,'register']);
Route::post('/v1/refresh', [AdminController::class,'RefreshToken']);
Auth::routes();

// Rutas que solo requieren autenticacion.
Route::middleware(['auth:api'])->group(function () {

    Route::post('/v1/logout', [AdminController::class,'cerrarSesionActual']); 
    Route::post('/v1/logouts', [AdminController::class,'cerrarSesiones']); 
    Route::get('/v1/perfil/listado-rutas', [AdminController::class,'listarRutasUsuario']);
    Route::get('/v1/perfil', [AdminController::class,'obtenerMisDatos']);
    Route::put('/v1/perfil', [AdminController::class,'actualizarMisDatos']);
    Route::patch('/v1/perfil/password',[AdminController::class,'actualizarMiPassword']);
    Route::patch('/v1/perfil/nombre', [AdminController::class,'actualizarMiNombre']);
    Route::patch('/v1/perfil/email', [AdminController::class,'actualizarMiEmail']);
    Route::patch('/v1/perfil/apellido', [AdminController::class,'actualizarMiApellido']);
    Route::patch('/v1/perfil/usuario', [AdminController::class,'actualizarMiNombreUsuario']);
    Route::patch('/v1/perfil/descripcion', [AdminController::class,'actualizarMiDescripcion']);

});
// can:admin-users es un Gate definido en AuthServiceProvider, verifica si el usuario pertenece al grupo Administradores

// Rutas autenticadas que pertenezcan al grupo de Administradores
Route::middleware(['auth:api'])->group(function () {

    Route::put('/v1/admin/usuarios/habilitado',[AdminController::class,'activarUsuario'])->middleware("can:acceso-ruta");
    Route::put('/v1/admin/usuarios/deshabilitado',[AdminController::class,'desactivarUsuario'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios/email/{email}',[AdminController::class,'obtenerUsuarioEmail'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios/usuario/{nombre}',[AdminController::class,'obtenerUsuarioNombre'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios/apellido/{apellido}',[AdminController::class,'obtenerUsuarioApellido'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios', [AdminController::class,'listarUsuarios'])->middleware("can:acceso-ruta");
    Route::post('/v1/admin/usuarios',[AdminController::class,'creaUsuario'])->middleware("can:acceso-ruta");
    Route::put('/v1/admin/usuarios/{id_usuario}',[AdminController::class,'actualizaUsuario'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/usuarios/{id_usuario}',[AdminController::class,'borrarUsuario'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/usuarios/{id_usuario}',[AdminController::class,'obtenerUsuarioId'])->middleware("can:acceso-ruta");
    Route::patch('/v1/admin/usuarios/{id_usuario}/nombre',[AdminController::class,'actualizarNombreUsuario'])->middleware("can:acceso-ruta");
    Route::patch('/v1/admin/usuarios/{id_usuario}/email',[AdminController::class,'actualizarEmailUsuario'])->middleware("can:acceso-ruta");
    Route::patch('/v1/admin/usuarios/{id_usuario}/apellido',[AdminController::class,'actualizarApellidoUsuario'])->middleware("can:acceso-ruta");
    Route::patch('/v1/admin/usuarios/{id_usuario}/usuario',[AdminController::class,'actualizarUserNameUsuario'])->middleware("can:acceso-ruta");
    Route::patch('/v1/admin/usuarios/{id_usuario}/descripcion',[AdminController::class,'actualizarDescripcionUsuario'])->middleware("can:acceso-ruta");
    Route::patch('/v1/admin/usuarios/{id_usuario}/password', [AdminController::class,'cambiarPasswordUsers'])->middleware("can:acceso-ruta");

    Route::post('/v1/admin/grupos',[AdminController::class,'crearGrupo'])->middleware("can:acceso-ruta");
    Route::put('/v1/admin/grupos/{id_grupo}',[AdminController::class,'actualizarGrupo'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/grupos/{id_grupo}',[AdminController::class,'borrarGrupo'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos', [AdminController::class,'listarGrupos'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos/usuarios',[AdminController::class,'obtenerUsuariosGrupos'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos/rutas',[AdminController::class,'obtenerGruposRutas'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos/{id_grupo}', [AdminController::class,'obtenerGrupo'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos/nombre/{nombre}', [AdminController::class,'obtenerGrupoNombre'])->middleware("can:acceso-ruta");

    Route::post('/v1/admin/rutas',[AdminController::class,'crearRuta'])->middleware("can:acceso-ruta");
    Route::put('/v1/admin/rutas/{id_ruta}',[AdminController::class,'actualizarRuta'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/rutas/{id_ruta}',[AdminController::class,'borrarRuta'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/rutas', [AdminController::class,'listarRutas'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/rutas/{id_ruta}', [AdminController::class,'obtenerRuta'])->middleware("can:acceso-ruta");

    Route::delete('/v1/admin/rutasmetodo/{id_rutametodo}',[AdminController::class,'borrarRutaMetodo'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/rutasmetodo',[AdminController::class,'borrarRutaMetodo1'])->middleware("can:acceso-ruta");
    
    Route::post('/v1/admin/grupos/{id_grupo}/usuarios/{id_usuario}',[AdminController::class,'crearUsuarioGrupo'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/grupos/{id_grupo}/usuarios/{id_usuario}',[AdminController::class,'eliminarUsuarioGrupo'])->middleware("can:acceso-ruta");
    
    Route::get('/v1/admin/usuarios/{id_usuario}/grupos',[AdminController::class,'obtenerGruposDeUsuario'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos/{id_grupo}/usuarios',[AdminController::class,'obtenerUsuariosDeGrupo'])->middleware("can:acceso-ruta");

    Route::post('/v1/admin/grupos/{id_grupo}/rutas/{id_ruta}',[AdminController::class,'crearGrupoRuta'])->middleware("can:acceso-ruta");
    Route::delete('/v1/admin/grupos/{id_grupo}/rutas/{id_ruta}',[AdminController::class,'eliminarGrupoRuta'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/grupos/{id_grupo}/rutas',[AdminController::class,'obtenerRutasDeGrupo'])->middleware("can:acceso-ruta");
    Route::get('/v1/admin/rutas/{id_ruta}/grupos',[AdminController::class,'obtenerGruposDeRuta'])->middleware("can:acceso-ruta");

});

//Las funcines de rutas a una base de datos externas, deben colocarse en ApiController
//Ruta de Ejemplo
/*Route::middleware(['auth:sanctum'])->group(function () {

    Route::get('/v2/prueba/administrativos', [ApiController::class,'listarAdministrativos'])->middleware("can:acceso-ruta");

});
*/