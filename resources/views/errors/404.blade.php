@extends('errors.minimal')

@section('title', @$message ?? '')
@section('code', '404')
@section('message', @$message ?? '')


