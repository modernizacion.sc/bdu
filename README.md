# Bases de Datos Unificadas de la Provincia de Santa Cruz

> Creada por Resolución 034/2020, Jefatura de Gabinete de Ministros de la Provincia de Santa Cruz.


# Contenido
- [Versiones](#Versiones)
- [Instalación como Contenedor Docker](#instalación-como-contenedor-docker)
- [Instalación Manual](#instalación-manual)
- [Resolución de Problemas](#resolución-de-problemas)
- [Documentación de Desarrollador](#documentación-de-desarrollador)
- [Documentación de Usuario](#documentación-de-usuario)

# Versiones
| Fecha | Comentarios |
| ------ | ------ |
| 25/04/24 | Actualización a Debian 12 Bookworm, PHP 8.3 y MySQL 8.3.0 |
|        |        |
|        |        |


# Instalación como Contenedor Docker
A continuación se enumeran los pasos para la instalación del software BDU como contenedor Docker.

## Requerimientos
- Sistema Operativo GNU/Linux
- Git
- Docker
- Docker Compose

## Descarga del proyecto
```bash
$ cd ~
$ git clone https://gitlab.com/modernizacion.sc/bdu.git bdu
```
## Selección de tipo de despliegue
### Opción 1. Contenedor Nginx como Proxy Reverso + Contenedor Apache-PHP-BDU + Contenedor MySQL + Contenedor PHPMyAdmin
- Utilizar el archivo `docker-compose.yml` provisto por defecto
- Editar el archivo `~bdu/nginx/nginx.conf` y definir la variable `server_name` correspondiente a su dominio.

### Opción 2. Contenedor Apache-PHP-BDU + Contenedor MySQL
- Utilizar el archivo `docker-compose.yml` opcional:
```bash
$ cd ~/bdu
$ cp docker-compose.yml.opc docker-compose.yml
```

## Configuración de variables de entorno
```bash
$ cd ~/bdu
$ cp .env-docker.example .env
```
- Editar el archivo `~/bdu/.env` y ajustar, de ser necesario, las siguientes variables:
	- Datos de acceso de administrador (necesarios para la creación de la base de datos y usuario):
		- `MYSQL_ADMIN_USERNAME`
		- `MYSQL_ROOT_PASSWORD`
	- Usuario de base de datos BDU:
		- `MYSQL_USER`
		- `MYSQL_PASSWORD`
	- Datos de acceso a la base de datos local que interactuará con el API:
		- `LOCAL_DB_HOST`
		- `LOCAL_DB_PORT`
		- `LOCAL_DB_DATABASE`
		- `LOCAL_DB_USERNAME`
		- `LOCAL_DB_PASSWORD`
	- Datos de configuración de cuenta de correo para validación de usuarios y recuperación de contraseña, en caso de que se utilicen estas opciones:
		- `MAIL_HOST`
		- `MAIL_PORT`
		- `MAIL_USERNAME`
		- `MAIL_PASSWORD`
		- `MAIL_ENCRYPTION`
		- `MAIL_FROM_ADDRESS`
	- Datos de configuración de Google Recaptcha, en caso de que se utilice esta opción en registros de usuarios de acceso público:
		- `RECAPTCHA_SITE_KEY`
		- `RECAPTCHA_SECRET_KEY`

## Creación del contenedor
```bash
$ cd ~/bdu
$ sudo docker-compose build bdu
$ sudo docker-compose up -d bdu bdu_db
$ sudo docker exec -t bdu composer install --no-dev -d /var/www/bdu
$ sudo docker exec -t bdu php /var/www/bdu/artisan key:generate
$ sudo docker exec -t bdu php /var/www/bdu/artisan passport:install --force
$ sudo docker exec -t bdu php /var/www/bdu/artisan passport:keys
$ sudo chmod -R 777 ~/bdu/storage 
$ sudo docker-compose restart bdu
```

# Instalación Manual
A continuación se enumeran los pasos para la instalación del software BDU bajo sistema operativo GNU/Linux

## Requerimientos
- Sistema Operativo GNU/Linux (Verificado para Debian 12.5 Bookworm)
- Servidor Web Apache
- PHP 8.3+
- Mysql 8.3.0+
- PHP Composer 2.7.4+

## Preparación e instalación de paquetes varios:
```bash
$ sudo apt update
$ sudo apt upgrade
$ sudo apt install git nano wget curl software-properties-common gnupg2
```
## Instalación de MySQL 8
```bash
$ wget https://dev.mysql.com/get/mysql-apt-config_0.8.16-1_all.deb
$ sudo dpkg –i mysql-apt-config_0.8.16-1_all.deb
$ sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 3A79BD29
$ sudo apt update
$ sudo apt install mysql-server
	Contraseña: A definir por el administrador.
	Use Strong Password Encryption
$ sudo systemctl enable mysql
```
## Instalación de Servidor Web Apache y PHP 8.3
```bash
$ sudo apt install apt-transport-https lsb-release ca-certificates
$ echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
$ sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
$ sudo apt update
$ sudo apt install apache2 php8.3 libapache2-mod-php8.3 php8.3-{mysql,xml,bcmath,bz2,intl,gd,mbstring,zip}
```

## Instalación de Composer
```bash
$ sudo wget -O composer-setup.php https://getcomposer.org/installer
$ sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
```
## Descarga e instalación del Software BDU
```bash
$ cd /var/www
$ git clone https://gitlab.com/modernizacion.sc/bdu.git
$ cd bdu
$ cp .env.example .env
$ composer install --no-dev
$ php artisan key:generate
$ sudo chown –R www-data:www-data /var/www/bdu
$ sudo chmod –R 777 /var/www/bdu/storage
```
## Creación de Base de Datos, usuario y migración de Tablas MySQL
- Editar el archivo `/var/www/bdu/.env` y ajustar las siguientes variables:
	- Datos de acceso de administrador (necesarios para la creación de la base de datos y usuario):
		- `MYSQL_ADMIN_USERNAME`
		- `MYSQL_ROOT_PASSWORD`
	- Usuario de base de datos BDU:
		- `MYSQL_USER`
		- `MYSQL_PASSWORD`
	- Datos de acceso a la base de datos local que interactuará con el API:
		- `LOCAL_DB_HOST`
		- `LOCAL_DB_PORT`
		- `LOCAL_DB_DATABASE`
		- `LOCAL_DB_USERNAME`
		- `LOCAL_DB_PASSWORD`
	- Datos de configuración de cuenta de correo para validación de usuarios y recuperación de contraseña, en caso de que se utilicen estas opciones:
		- `MAIL_HOST`
		- `MAIL_PORT`
		- `MAIL_USERNAME`
		- `MAIL_PASSWORD`
		- `MAIL_ENCRYPTION`
		- `MAIL_FROM_ADDRESS`
	- Datos de configuración de Google Recaptcha, en caso de que se utilice esta opción en registros de usuarios de acceso público:
		- `RECAPTCHA_SITE_KEY`
		- `RECAPTCHA_SECRET_KEY`
		
```bash
$ cd /var/www/bdu
$ sudo chmod +x ./setup_mysql.sh
$ ./setup_mysql.sh
```
## Configuración de Passport
```bash
$ sudo php artisan passport:install
```

## Configuración de Virtual Host Apache
```bash
$ sudo cp /var/www/bdu/apache/sites-available/bdu.conf /etc/apache2/sites-available
$ sudo cp /var/www/bdu/apache/sites-available/bdu-ssl.conf /etc/apache2/sites-available 
$ sudo rm /etc/apache2/sites-enabled/*
$ sudo ln –s /etc/apache2/sites-available/bdu.conf /etc/apache2/sites-enabled/
$ sudo ln –s /etc/apache2/sites-available/bdu-ssl.conf /etc/apache2/sites-enabled/
$ sudo a2enmod rewrite
$ sudo systemctl restart apache2
```
# Resolución de Problemas
## Desinstalación de software de base de datos existente (MySQL o MariaDB)
- En caso de contar con una versión previa de MySQL o MariaDB instalada en el sistema, se podrá desinstalar siguiendo los siguientes pasos:
```bash
$ sudo apt remove --purge mysql-\*
$ sudo apt remove --purge mariadb-\*
$ sudo apt --fix-broken install
$ sudo apt autoremove
```
## Creación manual de base de datos BDU
- En caso de requerirlo, se dispone de los scripts correspondientes a la creación de la base de datos, usuario de administración, tablas y datos iniciales en `/var/www/bdu/mysql/extras` y `/var/www/bdu/mysql/initdb.d`
## Desinstalación de software PHP existente
- En caso de contar con la versión 8.3 de PHP, es posible desinstalarlo mediante el siguiente comando:
```bash
$ sudo apt remove libapache2-mod-php8.3 php8.3
```

## Habilitar PHP 8.3 
Para que Apache use la version instalada de PHP 8.3 utilizamos el siguiente comando:
```bash
$ sudo a2enmod php8.3
```
Luego reiniciamos Apache para activar la nueva configuración, mediante el siguiente comando:
```bash
$ sudo systemctl restart apache2
```

## Sudo: no se puede resolver el host (unable to resolve host explained)
Por lo general, este error se produce después de cambiar el nombre de host de su sistema . El nombre de host es el nombre de su dispositivo con el que se identifica en la red. Se almacena localmente en el archivo `/etc/hostname`.

 - Verificamos el nombre del host, mediante el comando:
```bash
$ hostname
```  
- Iniciamos sesión como root, mediante el comando:
```bash
$ sudo su
```  
- Editamos el contenido del archivo `/etc/hosts`, mediante el comando:
```bash
$ nano /etc/hosts 
``` 
donde agregaremos la siguiente linea `127.0.0.1 nombre_host`, donde `nombre_host` es el nombre que se obtuvo mediante el comando `hostname` y guardamos los cambios realizados.

## Reconstrucción de los Contenedores Docker
### Contenedor MySQL 
Para regenerar el proyecto desde cero, incluyendo la creación de tablas de MySQL, se deberá eliminar manualmente el contenido del directorio `bdu/mysql/data`:
```bash
$ cd ~
$ sudo rm -rf bdu/mysql/data/*
```
- Reconstruir el contenedor:
```bash
$ cd ~/bdu
$ sudo docker-compose build bdu_bd
$ sudo docker-compose up -d bdu_db
```
### Contenedor Servidor Web Apache, PHP y proyecto
- Reconstruir el contenedor:
```bash
$ cd ~/bdu
$ sudo docker-compose build bdu
$ sudo docker-compose restart bdu
```
En el caso de modificaciones en las dependencias del proyecto que requieran la regeneración del mismo:
```bash
$ cd ~/bdu
$ sudo docker exec -t bdu composer install --no-dev -d /var/www/bdu
$ sudo docker-compose up -d bdu
```

# Documentación de Desarrollador
## Configuración de bases de datos adicionales
- Para generar conexiones a bases de datos adicionales se deberán incluir un apartado para el controlador correspondiente dentro del arreglo `connections` del archivo `bdu/config/database.php`.
- Los datos de conexión (host, puerto, usuario, contraseña) se establecerán en el archivo `bdu/.env`.

## Definición de Rutas
Las rutas disponibles para el API se deben definir en `bdu/routes/web.php`. Se han definido cuatro secciones:
- Rutas públicas, accesibles por todos los usuarios sin requerir autenticación.
- Rutas para todos los usuarios autenticados.
- Rutas para usuarios administradores. Aquí se definen por defecto las rutas correspondientes a tareas de administración del API. 
- Rutas locales, donde se sefinirán las rutas específicas de los recursos compartidos desde el API.

Para detalles de utilización del API ver la [Documentación de Usuario](#documentación-de-usuario)

## Configuración de Controladores
Toda la lógica de programación de las rutas se define a través de controladores. 
- Las funciones específicas relacionadas a las rutas administrativas se encuentran en `bdu/app/Http/Controllers/AdminController.php`.
- Las funciones correspondientes al API se establecen en `bdu/app/Http/Controllers/ApiController.php`.

## Repositorio de Conexiones a Bases de Datos
- La conexión a la base de datos BDU y consultas SQL administrativas se establecen `bdu/app/Repository/AdminRepository`.
- Las conexiones y consultas a las bases de datos locales se establecen en `bdu/app/Repository/ApiRepository`.

# Documentación de Usuario

## Colección Postman
Postman es una herramienta que permite crear y probar peticiones sobre APIs de forma sencilla. Puede obtenerse gratuitamente desde este enlace:
- [Descargar la aplicación POSTMAN](https://www.postman.com/downloads/)

Las rutas disponibles por defecto en BDU se encuentran agrupadas en una colección POSTMAN:
- [Descargar colección BDU para POSTMAN](BDU.postman_collection.json)

> Dentro de Postman definir las variables `{{HOST}}`,`{{TOKEN}}` y `{{RTOKEN}}` para la correcta utilización de la colección.

## Perfil Administrador
El sistema cuenta con un usuario por defecto con privilegios administrativos:
- Usuario: **admin**
- Grupo: **Administradores**
- Contraseña inicial: **admin**

## Rutas Públicas
| Descripción | Método | Ruta |
| :----------- | :------: | :---- |
| Login | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/login
| Registrar nuevo usuario | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/register
| Restablecer Contraseña | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/restablecimiento
| Renovar token | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/refresh
| Recuperar contraseña | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/recuperar

## Rutas para usuarios autenticados
| Descripción | Método | Ruta |
| :----------- | :------: | :---- |
| Modificar mi nombre | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/perfil/nombre |
| Modificar mi nombre de usuario | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/perfil/usuario |
| Modificar mi email | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/perfil/email |
| Modificar mi apellido | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/perfil/apellido |
| Modificar mi descripcion | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/perfil/descripcion |
| Modificar mi contraseña | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/perfil/password |
| Listar rutas accesibles por usuario | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/perfil/listado-rutas |
| Obtener perfil de usuario | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/perfil |
| Modificar perfil de usuario | ![PUT](https://img.shields.io/badge/%20-PUT-blue) | /v1/perfil |
| Cerrar sesion actual | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/logout |
| Cerrar todas las sesiones | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/logouts |
## Rutas restringidas a Administradores
| Descripción | Método | Ruta |
| :----------- | :------: | :---- |
| Crear usuario |![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/admin/usuarios |
| Modificar usuario | ![PUT](https://img.shields.io/badge/%20-PUT-blue) | /v1/admin/usuarios/{id_usuario} |
| Eliminar usuario | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/usuarios/{id_usuario} |
| Activar usuario | ![PUT](https://img.shields.io/badge/%20-PUT-blue) | /v1/admin/usuarios/habilitado |
| Desactivar usuario | ![PUT](https://img.shields.io/badge/%20-PUT-blue) | /v1/admin/usuarios/deshabilitado |
| Obtener usuarios | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/usuarios |
| Obtener usuario por ID | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/usuarios/{id_usuario} |
| Obtener usuario por email| ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/usuarios/email/{email} |
| Obtener usuario por nombre| ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/usuarios/usuario/{name} |
| Obtener usuario por apellido| ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/usuarios/apellido/{apellido} |
| Modificar usuario - Nombre | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/admin/usuarios/{id}/nombre |
| Modificar usuario - Email | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/admin/usuarios/{id}/email |
| Modificar usuario - Apellido | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/admin/usuarios/{id}/apellido |
| Modificar usuario - Nombre Usuario | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/admin/usuarios/{id}/usuario |
| Modificar usuario - Descripcion | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/admin/usuarios/{id}/descripcion |
| Modificar contraseña de usuario | ![PATCH](https://img.shields.io/badge/%20-PATCH-gray) | /v1/admin/usuarios/{id_usuario}/password |
| Crear grupo | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/admin/grupos |
| Modificar grupo | ![PUT](https://img.shields.io/badge/%20-PUT-blue) | /v1/admin/grupos/{id_grupo} |
| Eliminar grupo | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/grupos/{id_grupo} |
| Obtener grupos | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/grupos |
| Obtener grupo por ID | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/grupos/{id_grupo} |
| Obtener grupo por su nombre | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/grupos/nombre/{nombre} |
| Agregar usuario a grupo | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/admin/grupos/{id_grupo}/usuarios/{id_usuario}|
| Eliminar usuario de grupo | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/grupos/{id_grupo}/usuarios/{id_usuario} |
| Obtener grupos con usuarios | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/grupos/usuarios |
| Obtener usuarios de un grupo | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/grupos/{id_grupo}/usuarios |
| Obtener grupos de un usuario | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/usuarios/{id_usuario}/grupos |
| Crear ruta | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/admin/rutas |
| Modificar ruta | ![PUT](https://img.shields.io/badge/%20-PUT-blue) | /v1/admin/rutas/{id_ruta} |
| Eliminar ruta | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/rutas/{id_ruta} |
| Eliminar ruta por metodo | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/rutasmetodo |
| Eliminar ruta por metodo | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/rutasmetodo/{id_gruporutametodo} |
| Obtener rutas | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/rutas |
| Obtener ruta por ID | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/rutas/{id} |
| Agregar ruta a un grupo | ![POST](https://img.shields.io/badge/%20-POST-yellow) | /v1/admin/grupos/{id_grupo}/rutas/{id_ruta} |
| Eliminar ruta de un grupo | ![DELETE](https://img.shields.io/badge/%20-DELETE-critical) | /v1/admin/grupos/{id_grupo}/rutas/{id_ruta} |
| Obtener todos los grupos con rutas | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/grupos/rutas |
| Obtener rutas de un grupo | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/grupos/{id_grupo}/rutas |
| Obtener grupos de una ruta | ![GET](https://img.shields.io/badge/%20-GET-green) | /v1/admin/rutas/{id_ruta}/grupos |
 
